<%--
  Created by IntelliJ IDEA.
  User: pawel
  Date: 2018-10-08
  Time: 18:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Event Add</title>
</head>
<body>
<form action="/event/add" method="post">
    <div>
        <label for="name">Name:</label>
        <input id="name" name="name" type="text`">
    </div>
    <div>
        <label for="description">Description:</label>
        <input id="description" name="description" type="text">
    </div>
    <div>
        <label for="time">Time:</label>
        <input id="time" name="time" type="datetime-local">
    </div>
    <div>
        <label for="length">Length (in minutes):</label>
        <input id="length" name="length" type="number">
    </div>
    <input type="submit" value="Register">


</body>
</html>
