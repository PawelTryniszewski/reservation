<%--
  Created by IntelliJ IDEA.
  User: pawel
  Date: 2018-10-08
  Time: 17:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User Name</title>
</head>
<body>
<h2>User form:</h2>
<span style="color: red; ">
<c:out value="${error_message}"/>
</span>
<form action="/user/register" method="post">
    <div>
        <label for="email">User Name:</label>
        <input id="email" name="email" type="email">
    </div>
    <div>
        <label for="username">User Name:</label>
        <input id="username" name="username" type="text">
    </div>
    <div>
        <label for="password">Password:</label>
        <input id="password" name="password" type="password">
    </div>
    <div>
        <label for="confirmpassword">Confirm Password:</label>
        <input id="confirmpassword" name="confirmpassword" type="password">
    </div>
    <input type="submit" value="Register">

</form>

</body>
</html>
